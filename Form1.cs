﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.Entity;
using EntityFrameworkTest;

namespace EntityFrameworkTest {
    public partial class Form1 : Form {
        wsrUsers db;
        int lastId = 0;
        public Form1() {
            InitializeComponent();
            db = new wsrUsers();
            db.Users.Load();
            dataGridView1.DataSource = db.Users.Local.ToBindingList();
            if (dataGridView1.RowCount != 0)
                lastId = (int)dataGridView1[0, dataGridView1.RowCount - 1].Value;
        }

        private void button1_Click(object sender, EventArgs e) {
            UserForm form = new UserForm();
            form.button1.Text = "Add";
            form.numericUpDown1.Value = lastId + 1;
            if (form.ShowDialog(this) == DialogResult.Cancel)
                return;
            User user = new User();
            user.id = (int) form.numericUpDown1.Value;
            user.login = form.textBox1.Text;
            user.password = form.textBox2.Text;

            db.Users.Add(user);
            db.SaveChanges();
            lastId = (int)dataGridView1[0, dataGridView1.RowCount - 1].Value;
            MessageBox.Show("Added!");
        }

        private void button2_Click(object sender, EventArgs e) {
            if (dataGridView1.SelectedRows.Count == 0) {
                MessageBox.Show("No row selected!");
                return;
            }
            int index = dataGridView1.SelectedRows[0].Index;
            int id = 0;
            bool converted = Int32.TryParse(dataGridView1[0, index].Value.ToString(), out id);
            if (converted == false)
                return;
            User user = db.Users.Find(id);

            UserForm form = new UserForm();
            form.button1.Text = "Add";
            form.numericUpDown1.Value = user.id;
            form.textBox1.Text = user.login;
            form.textBox2.Text = user.password;

            if (form.ShowDialog(this) == DialogResult.Cancel)
                return;
            
            user.login = form.textBox1.Text;
            user.password = form.textBox2.Text;

            db.SaveChanges();
            dataGridView1.Refresh();
            MessageBox.Show("Modified!");
        }

        private void button3_Click(object sender, EventArgs e) {
            if (dataGridView1.SelectedRows.Count == 0) {
                MessageBox.Show("No row selected!");
                return;
            }
            int index = dataGridView1.SelectedRows[0].Index;
            int id = 0;
            bool converted = Int32.TryParse(dataGridView1[0, index].Value.ToString(), out id);
            if (converted == false)
                return;
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            if(dataGridView1.RowCount != 0)
                lastId = (int)dataGridView1[0, dataGridView1.RowCount - 1].Value;
            MessageBox.Show("Removed!");
        }
    }
}
